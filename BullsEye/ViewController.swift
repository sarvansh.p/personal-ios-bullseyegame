//
//  ViewController.swift
//  BullsEye
//
//  Created by Sarvansh Prasher on 06/01/19.
//  Copyright © 2019 Sarvansh Prasher. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var currentValue = 0
    var targetValue = 0
    var difference = 0
    var points = 0
    var score = 0
    var round = 0
    
    @IBOutlet var targetLabel : UILabel!
    @IBOutlet var slider : UISlider!
    @IBOutlet var scoreLabel : UILabel!
    @IBOutlet var roundLabel : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentValue = lroundf(slider.value)
        
        startRound()
    }
    
    func updateLabels (){
        
        targetLabel.text = String(targetValue)
        
        scoreLabel.text = String(score)
        
        roundLabel.text = String(round)
    }
    
    func startRound (){
        
        round += 1
        
        targetValue = 1 + Int(arc4random_uniform(100))
        
        currentValue = 0
        
        slider.value = Float(currentValue)
        
        updateLabels()
    }
    
    @IBAction func sliderMoved( _ slider: UISlider){
        print("The slider value is \(slider.value)")
        
        currentValue = lroundf(slider.value)
    }

    
    func computeDifference (){
//        if(targetValue > currentValue){
//            difference = targetValue - currentValue
//        }
//        else if(currentValue > targetValue){
//            difference = currentValue - targetValue
//        }
//        else{
//        difference = 0
//        }
        
//        var difference = currentValue - targetValue
//
//        if(difference < 0){
//
//            difference = -difference
//        }
        
        
        let difference = abs(targetValue - currentValue)
        
        points = 100 - difference
        
        score += points
        
        
    }

    @IBAction func showAlert(){
        
        computeDifference()
        
        let message = "You have scored \(points) points"
        
        let title : String
        
        if(difference == 0){
            title = "Perfect!"
        }
        else if(difference < 5){
            title = "You almost had it !"
        }
        else if(difference < 10 ){
            title = "You were very close !"
        }
        else {
            title = "Not even close "
        }
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Awesome", style: .default, handler: nil)
        alert.addAction(action)
    
        present(alert, animated: true, completion: nil)
        
        startRound()
    }
    
    @IBAction func resetScore (){
        
        score = 0
        
        round = 0
        
        points = 0
        
        viewDidLoad()
    }
}

