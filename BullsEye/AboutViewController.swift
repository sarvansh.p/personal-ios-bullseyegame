//
//  AboutViewController.swift
//  BullsEye
//
//  Created by Sarvansh Prasher on 24/01/19.
//  Copyright © 2019 Sarvansh Prasher. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func closeWindow (){
        dismiss(animated: true, completion: nil)
    }

}
